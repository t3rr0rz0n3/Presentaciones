# Presentacions

Resumen de mis presentaciones.

## HackMeeting

Aquí se incluyen dos presentaciones que se hicieron en el HackMeetin de HackNova "Como deshacerte de Google" y la mesa redonda del IngoHack "Mesa Redonda de Podcasting"

## Seguretat Informàtica i militància

Xerrada feta a diferents col·lectius on s'explica com les empreses ens espien i recopilen informació nostra. A més de com fer que com a col·lectiu ens poguem protegir d'aquestes empreses.

# Licencia

GNU GENERAL PUBLIC LICENSE
Version 3, 29 June 2007

